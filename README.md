# Boatway

Welcome to the open source repository of Boatway. Boatway is the result of a graduation project executed by Christan Vermeulen during his graduation at Teqplay. The aim of this project is to improve the safety at the rivers, which are being used by both commercial ships and recreational ships. Commercial ships are equipped with a multitude of tools to improve safety and security, including mariphone, radar, GPS and AIS. Many small recreational boats do not have such tools on board, and are dependent on vision and focus.
Boatway is developed and tested as a tool for recreational skippers to make them aware of the commercial ships, by presenting the AIS information in multiple ways:

* On the map, to create situational awareness of the environment
* In a list, indicating which commercial ships you will encounter in how much time

Based on the research being executed (for more details, results of questionnaires etc. see http://afstuderen.christianvermeulen.net/) this prototype has been developed using the Ionic framework, and tested with multiple recreational skippers.

# Open source

To improve safety the source code of this project has been open sourced. Please keep a reference to both the developer Christian Vermeulen (http://christianvermeulen.net ) and Teqplay (http://www.teqplay.nl) the company running the project and coaching him as a graduate.

# License

Copyright (c) 2015 Teqplay b.v.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.